
describe('.registry()', function () {
  it('should get the registry', co.wrap(function* () {
    var registry = yield* client.registry()
    assert(registry.registry)
    assert(registry.server)
  }))

  // terrible test!
  it('should save the proxy file', co.wrap(function* () {
    yield fs.stat(client._registryCacheFilename)
  }))

  it('should get the cache locally', co.wrap(function* () {
    client.close()
    client._registry = null
    var registry = yield* client.registry()
    assert(!client.agent)
  }))
})
