
describe('.resolve()', function () {
  describe('should resolve specific versions without HTTP requests', function () {
    it('jonathanong/horizontal-grid-packing@0.1.4', co.wrap(function* () {
      var uri = yield* client.resolve('/github/jonathanong/horizontal-grid-packing/0.1.4/index.js')
      assert.equal(uri, '/github/jonathanong/horizontal-grid-packing/0.1.4/index.js')
      assert(yield fs.exists(client.directory + '/github/jonathanong/horizontal-grid-packing/0.1.4/index.js'))
      assert(yield fs.exists(client.directory + '/github/jonathanong/horizontal-grid-packing/6a61b2ae95dca5546669b03e1ce7bb9789985ba4/index.js'))
      client.close()
    }))

    it('jonathanong/horizontal-grid-packing@v0.1.4', co.wrap(function* () {
      var uri = yield* client.resolve('/github/jonathanong/horizontal-grid-packing/v0.1.4/index.js')
      assert.equal(uri, '/github/jonathanong/horizontal-grid-packing/0.1.4/index.js')
      assert(!client.agent)
    }))

    it('jonathanong/horizontal-grid-packing@=0.1.4', co.wrap(function* () {
      var uri = yield* client.resolve('/github/jonathanong/horizontal-grid-packing/=0.1.4/index.js')
      assert.equal(uri, '/github/jonathanong/horizontal-grid-packing/0.1.4/index.js')
      assert(!client.agent)
    }))

    it('component-emitter@1', co.wrap(function* () {
      yield* client.resolve('/npm/-/component-emitter/1/index.js')
    }))
  })

  describe('should download all the dependencies', function () {
    it('jonathanong/textcomplete@0.0.1', co.wrap(function* () {
      var uri = yield* client.resolve('/github/jonathanong/textcomplete/0.0.1/lib/index.js')
      yield fs.stat(client.directory + '/github/component/query/0.0.3/index.js')
    }))
  })

  it('should resolve normalize.css', co.wrap(function* () {
    var uri = yield* client.resolve('/github/necolas/normalize.css/3/index.css');
  }))
})
