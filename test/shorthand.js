
describe('.shorthand()', function () {
  describe('npm', function () {
    it('emitter@1', co.wrap(function* () {
      var url = yield* client.shorthand('emitter@1')
      assert.equal(url, 'https://nlz.io/npm/-/emitter/1/index.js')
    }))
  })

  describe('github', function () {
    it('component/emitter@1', co.wrap(function* () {
      var url = yield* client.shorthand('component/emitter@1')
      assert.equal(url, 'https://nlz.io/github/component/emitter/1/index.js')
    }))
  })
})
