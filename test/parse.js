
describe('.parse()', function () {
  it('should normalize cases', function () {
    assert.equal(
      client.parse('/github/Swatinem/Something/0.0.1/index.js').pathname,
      '/github/swatinem/something/0.0.1/index.js'
    )

    assert.equal(
      client.parse('/npm/-/JSONStream/1.0.0/index.js').pathname,
      '/npm/-/JSONStream/1.0.0/index.js'
    )
  })

  describe('should match', function () {
    [
      ['/github/component/emitter', '/github/component/emitter'],
      ['/github.com/component/emitter', '/github/component/emitter'],
      ['https://github.com/component/emitter', '/github/component/emitter'],
      ['https://github.com/component/emitter/1.0.0/index.js', '/github/component/emitter/1.0.0/index.js'],
      ['https://nlz.io/github.com/component/emitter/1.0.0/index.js', '/github/component/emitter/1.0.0/index.js'],
      ['https://nlz.io/github/component/emitter/1.0.0/index.js', '/github/component/emitter/1.0.0/index.js'],
    ].forEach(function (pair) {
      it(pair[0] + ' -> ' + pair[1], function () {
        assert.equal(client.parse(pair[0]).pathname, pair[1])
      })
    })
  })

  describe('should not match', function () {
    [
      'http://github.com/component/emitter',
      'https://github.com/component',
      'http://facebook.com/component/emitter',
      'http://nlz.io/facebook/component/emitter',
    ].forEach(function (url) {
      it(url + ' -> false', function () {
        assert.equal(false, client.parse(url))
      })
    })
  })

  it('should return the version and filename', function () {
    var uri = client.parse('https://github.com/component/emitter/^1.0.0/lib/index.js')

    assert.equal(uri.version, '%5E1.0.0')
    assert.equal(uri.filename, 'lib/index.js')
    assert.equal(uri.pathname, '/github/component/emitter/%5E1.0.0/lib/index.js')

    assert.equal(client.format(uri), '/github/component/emitter/%5E1.0.0/lib/index.js')
  })
})
