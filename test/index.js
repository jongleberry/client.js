
co = require('co')
fs = require('mz/fs')
assert = require('assert')
rimraf = require('rimraf')
resolve = require('path').resolve

var Client = require('..')

rimraf.sync(resolve('cache'))
var port = process.env.PORT || (process.env.PORT = 6123)
port = port | 0

before(function (done) {
  server = require('normalize-registry/app/server.js').listen(port, function (err) {
    if (err) return done(err)

    client = new Client({
      port: port,
      rejectUnauthorized: false,
    })

    done()
  })
})

require('./registry')
require('./shorthand')
require('./parse')
require('./versions')
require('./resolve')
