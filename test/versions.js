
describe('.versions()', function () {
  it('should return the latest version if not installed', co.wrap(function* () {
    var versions = yield* client.versions('/github/component-test/remotes')
    assert(versions.length)
    assert.equal(versions[0][0], '0.0.0')

    var versions = yield* client.versions('/github/jonathanong/horizontal-grid-packing')
    assert(~versions.map(toVersion).indexOf('0.1.4'))
  }))

  it('should get the versions of an npm project', co.wrap(function* () {
    var versions = yield* client.versions('/npm/-/component-emitter')
    assert(~versions.map(toVersion).indexOf('1.1.3'))
  }))
})

function toVersion(x) {
  return x[0]
}
