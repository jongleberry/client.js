
/**
 * Create raw requests, optionally automatically parsing JSON.
 */

var https = require('https')
var get = require('raw-body')
var error = require('http-errors')
var inflate = require('inflation')
var debug = require('debug')('normalize-agent:request')

var Client = require('./client')

/**
 * @param {Object|String} options|path
 * @param {String} type
 * @return {http.IncomingMessage} stream
 * @api private
 */

Client.prototype.request = function* (options, type) {
  if (typeof options === 'string') options = { path: options }

  var req = https.request({
    host: this.hostname,
    agent: this.open(),
    method: 'GET',
    path: options.path,
    headers: this.headers,
  })

  var res = yield function (done) {
    req.on('error', done)
    req.on('response', done.bind(null, null))
    req.end()
  }

  debug('%s got status code %s', options.path, res.statusCode)

  if (type !== 'json') return res

  if (Math.floor(res.statusCode / 100) === 3) {
    debug('got status code %s from %s, ignoring', res.statusCode, options.path)
    res.destroy()
    return
  }

  return yield* this.parseJSON(res)
}

Client.prototype.parseJSON = function* (res) {
  if (!/application\/json/.test(res.headers['content-type'])) {
    res.destroy()
    throw error(400, 'can not parse JSON of response of type: ' + res.headers['content-type'])
  }

  var body = yield get(inflate(res), true)

  try {
    body = JSON.parse(body)
  } catch (err) {
    err.status = 400
    throw err
  }

  res.body = body
  return res
}
