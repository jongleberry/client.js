
/**
 * This stores dependencies in
 *
 * .dependencies[<remote>:<user>/<project>] = {
 *   <version>: <real version> || true
 * }
 *
 * So you can Object.keys(dependencies[hash]) to find all versions
 * and lookup dependencies[hash][version] to look up the real version.
 * Future goal of this is to allow setting these dependencies manually
 * prior to resolution to manually overwrite dependencies, i.e. deduping.
 *
 * This currently does not support cleaning the cache, though
 * ideally it would just be `this.dependencies = {}` in the future.
 */

var fs = require('mz/fs')
var path = require('path')
var semver = require('semver')
var link = require('fs-symlink')

var Client = require('./client')

Client.prototype.hash = function (remote, user, project) {
  return remote.name + ':' + user + '/' + project
}

/**
 * If this.dependencies[hash] does not exist,
 * we first check the folder to read the versions.
 * This allows us to cache between builds but not
 * continuously read from the file system.
 *
 * However, multiple concurrent clients are not supported,
 * so things may fail if that happens.
 */

Client.prototype.readDependencies = function* (remote, user, project) {
  var hash = this.hash(remote, user, project)
  if (this.dependencies[hash]) return

  var folder = path.join(this.directory, remote.name, user, project)
  var dirs
  try {
    dirs = yield fs.readdir(folder)
  } catch (err) {
    // if there's no directory, just create an empty object
    this.dependencies[hash] = Object.create(null)
    return
  }

  var deps = this.dependencies[hash] = Object.create(null)
  yield dirs.map(function* (name) {
    var vers = path.join(folder, name)
    var stats = yield fs.lstat(vers)
    // it's just a version
    if (!stats.isSymbolicLink()) return deps[name] = true
    var real = yield fs.realpath(path.join(folder, name))
    real = path.relative(folder, real)
    deps[name] = real
    deps[real] = true
  })
}

Client.prototype.getMaxSatisfying = function (remote, user, project, range) {
  var hash = this.hash(remote, user, project)
  var cache = this.dependencies[hash]
  if (!cache) return []
  var versions = Object.keys(cache).filter(validVersion)
  return semver.maxSatisfying(versions, range)
}

Client.prototype.setDependencyAlias = function (remote, user, project, reference, alias) {
  var hash = this.hash(remote, user, project)
  var cache = this.dependencies[hash] = this.dependencies[hash] || Object.create(null)
  cache[alias] = reference
}

Client.prototype.linkDependencyAlias = function* (remote, user, project, reference, alias) {
  if (reference === alias) return
  this.setDependencyAlias(remote, user, project, reference, alias)
  var og = path.join(this.directory, remote.name, user, project, reference)
  var al = path.join(this.directory, remote.name, user, project, alias)
  yield link(og, al)
}

Client.prototype.getDependency = function (remote, user, project, version) {
  var hash = this.hash(remote, user, project)
  var cache = this.dependencies[hash]
  if (!cache) return
  var real = cache[version]
  if (!real) return
  return real === true ? version : real
}

function validVersion(x) {
  return semver.valid(x)
}
