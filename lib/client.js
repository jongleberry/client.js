
var co = require('co')
var spdy = require('spdy')
var resolve = require('path').resolve
var inherits = require('util').inherits
var debug = require('debug')('normalize-Client')
var EventEmitter = require('events').EventEmitter

module.exports = Client

inherits(Client, EventEmitter)

Client.prototype.await = require('await-event')

function Client(options) {
  if (!(this instanceof Client)) return new Client(options)

  options = options || {}
  EventEmitter.call(this, options)
  this.setMaxListeners(0)

  // caches
  // avoid multiple requests
  this.progress = Object.create(null)
  // store dependencies and their versions
  this.dependencies = Object.create(null)

  // Client listeners
  this.onerror = this.onerror.bind(this)
  this.onpush = this.onpush.bind(this)
  this.destroy = this.destroy.bind(this)

  // request options
  this.hostname = options.hostname || 'localhost'
  this.port = options.port || 443
  this.rejectUnauthorized = options.rejectUnauthorized !== false
  this.spdy = options.spdy || {}
  this.directory = resolve(options.directory || 'cache/normalize-client')
  this.headers = { // totally unnecessary
    'user-agent': 'https://github.com/normalize/Client.js',
  }

  if (options.auth)
    this.headers.authorization = 'Basic ' + new Buffer(options.auth).toString('base64')
}

// create a SPDY agent for this client
Client.prototype.open = function () {
  if (this.agent) return this.agent

  debug('opening agent')

  this.agent = spdy.createAgent({
    host: this.hostname,
    port: this.port,
    rejectUnauthorized: this.rejectUnauthorized,
    spdy: this.spdy
  })

  this.agent.on('error', this.onerror)
  // automatically download push streams
  this.agent.on('push', this.onpush)
  // automatically cleanup when the Client dies
  this.agent.on('close', this.destroy)

  this.emit('open')
  return this.agent
}

// destroy the spdy agent
Client.prototype.close =
Client.prototype.destroy = function () {
  if (!this.agent) return this

  debug('closing agent')

  this.agent.removeListener('error', this.onerror)
  this.agent.removeListener('push', this.onpush)
  this.agent.removeListener('close', this.destroy)
  this.agent.close()
  this.agent = null

  this.emit('close')
  return this
}

Client.prototype.onpush = function (stream) {
  debug('got push stream: ' + stream.url)
  co.call(this, this.download(stream)).catch(this.onerror)
}

Client.prototype.onerror = function (err) {
  if (!err) return
  this.emit('error', err)
  return this
}
