
var shorthands = require('normalize-shorthands')

var Client = require('./client')

Client.prototype.shorthands =
Client.prototype.shorthand = function* (str, ext) {
  var shorthand = this._shorthand
    || (this._shorthand = shorthands(yield* this.registry()))
  return shorthand(str, ext)
}
