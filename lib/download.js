
var fs = require('mz/fs')
var cp = require('fs-cp')
var path = require('path')
var assert = require('assert')
var parse = require('parse-link-header')
var debug = require('debug')('normalize-client:download')

var Client = require('./client')

/**
 * Downloads a file from a stream,
 * either a push stream or a response stream,
 * or a file string, with should be the pathname URL from the registry.
 *
 * These URLs should __always__ use the canonical URL from the registry.
 * In other words, for git repos, the version should be a commit sha.
 *
 * @param {http.IncomingMessage} stream
 * @return null
 * @api private
 */

Client.prototype.download = function* (stream) {
  /**
   * At this point, it should only be streams
   */

  var pathname = stream.url // push streams
    || (stream.req && stream.req.path) // http.IncomingMessage
  var out = this.parse(pathname)
  assert(pathname, 'WTF did not get a path')
  debug('downloading %s', pathname)

  // link versions
  var links = parse(stream.headers.link)
  if (links) {
    if (links.version) yield* this._linkVersions(out, links.version.url)
    else if (links.reference) yield* this._linkVersions(out, links.reference.url)
  }

  // already installed since it exists and a download isn't in progress
  var filename = path.resolve(this.directory, pathname.slice(1))
  if (yield fs.exists(filename)) {
    debug('file already exists: %s', filename)
    stream.resume()
    return
  }

  // downloading this file is already in progress,
  // so we'll wait until it's done to avoid race conditions
  // we shouldn't bother with making the request in the first place,
  // especially if the argument was a filename
  var event = 'download:' + pathname
  if (this.progress[event]) {
    debug('waiting for: %s', event)
    stream.resume()
    return yield this.await(event)
  }

  debug('downloading %s to %s', pathname, filename)

  this.progress[event] = true
  yield cp(stream, filename)

  debug('downloaded %s to %s', pathname, filename)

  delete this.progress[event]
  this.emit(event)
}

Client.prototype._linkVersions = function* (out, target) {
  var reference = target.split('/')[4]
  yield* this.linkDependencyAlias(out.remote, out.user, out.project, out.version, reference)
}
