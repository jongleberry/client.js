
var co = require('co')
var fs = require('mz/fs')
var path = require('path')
var assert = require('assert')
var mkdirp = require('mkdirp')
var memo = require('memorizer')
var debug = require('debug')('normalize-client:registry')

var Client = require('./client')

/**
 * Get /registry.json to return the valid remotes and stuff.
 * We store it to the `repositories` folder so people could
 * clean it with all the other files if necessary.
 * The main reason for this is that we don't want
 * users to need internet connection all the time
 * if they have everything locally.
 *
 * Note: this is overly complex and should be simplified somehow.
 */

// get the registry's metadata and cache it locally
Client.prototype.registry = function* () {
  if (this._registry) return this._registry
  if (this.progress.registry) return yield this.progress.registry
  return yield (this.progress.registry = co(this._getRegistry()))
}

Client.prototype._getRegistry = function* () {
  var registry = this._registry = (yield this._registryGetCache())
    || (yield this._registryGetRemote())
  delete this.progress.registry
  this.emit('registry', registry)
  return registry
}

// lookup registry.json locally
Client.prototype._registryGetCache = function () {
  return fs.readFile(this._registryCacheFilename, 'utf8')
    .then(JSON.parse).catch(returnFalse)
}

function returnFalse() {
  return false
}

// get the registry.json and save locally
Client.prototype._registryGetRemote = function* () {
  var res = yield* this.request('/registry.json', 'json')
  assert.equal(200, res.statusCode, 'something went wrong with /registry.json')
  var registry = res.body
  debug('writing %s', this._registryCacheFilename)
  yield mkdirp.bind(null, path.dirname(this._registryCacheFilename))
  yield fs.writeFile(this._registryCacheFilename, JSON.stringify(registry, null, 2))
  return registry
}

memo(Client.prototype, '_registryCacheFilename', function () {
  return path.resolve(this.directory,
    // only use port if necessary
    this.hostname + (this.port === 443 ? '' : '_' + this.port),
    'registry.json')
})
