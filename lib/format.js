
var Client = require('./client')

/**
 * Format the URI returned by .parse().
 * Allows overwriting and shit.
 *
 * @param {Object} uri
 * @return {String} path
 * @api public
 */

Client.prototype.format = function (uri) {
  var frags = ['', uri.remote.name, uri.user, uri.project]
  if (uri.version) frags.push(uri.version)
  if (uri.filename) frags.push(uri.filename)
  return frags.join('/')
}
