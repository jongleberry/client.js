
var fs = require('mz/fs')
var path = require('path')
var semver = require('semver')
var error = require('http-errors')
var parse = require('parse-link-header')
var debug = require('debug')('normalize-client:resolve')

var Client = require('./client')

/**
 * Resolve resolves the version of a URI string,
 * and returns the URI string with a version attached.
 * If the version is a specific version, nothing is downloaded.
 * That is delegated to .download().
 * Downloads only occur when resolving semver ranges.
 *
 * @param {String} uri
 * @return {Object}
 * @api public
 */

Client.prototype.resolve = function* (pathname) {
  var uri = this.parse(pathname)
  if (!uri) return false // invalid uri

  // lookup existing versions and references
  yield* this.readDependencies(uri.remote, uri.user, uri.project)

  // lookup an existing semver
  var isVersion = semver.valid(uri.version) || semver.validRange(uri.version)
  if (isVersion) {
    var version = this.getMaxSatisfying(uri.remote, uri.user, uri.project, uri.version)
    if (version) uri.version = version
  }

  pathname = this.format(uri)

  // if this particular file is already installed,
  // we don't need to do anything.
  var filename = path.resolve(this.directory, pathname.slice(1))
  if (yield fs.exists(filename)) return pathname

  // we'll now make a request and resolve the version this way
  var res = yield* this.request(pathname)
  switch (res.statusCode) {
    case 200:
      yield* this.download(res)
      return pathname
    case 301:
    case 302:
    case 303:
    case 307:
      res.resume()
      break // continue
    case 404:
      res.resume()
      throw error(404, 'File not found: ' + pathname)
    default:
      debug('received status %s for %s', res.statusCode, pathname)
      yield* this.parseJSON(res)
      throw error(res.statusCode, res.body.message)
  }

  var location = res.headers.location
  var links = parse(res.headers.link)

  res = yield* this.request(location)
  switch (res.statusCode) {
    case 200:
      break // continue
    default:
      // not sure why this would happen when we're following redirects
      debug('received status %s for %s', res.statusCode, pathname)
      yield* this.parseJSON(res)
      throw error(res.statusCode, res.body.message)
  }

  yield* this.download(res)

  if (links) {
    if (links.version) return links.version.url
    if (links.reference) return links.reference.url
  }
  return location
}
