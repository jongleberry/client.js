
var parse = require('url').parse
var assert = require('assert')

var Client = require('./client')

/**
 * Parse a URL.
 *
 * @param {String} URI
 * @return {Array|Object}
 * @api public
 */

Client.prototype.parse = function (uri) {
  var registry = this._registry
  assert(registry, 'you must call yield* client.registry() first')
  var remotes = registry.remotes
  var names = Object.keys(remotes)

  var url = parse(uri)
  if (url.protocol && url.protocol !== 'https:') return false
  var host = url.host || 'nlz.io'
  var path = url.pathname
  var remote
  var _remote

  // check if the source URL is used,
  // i.e. move host from hostname to path
  for (var i = 0; i < names.length; i++) {
    _remote = remotes[names[i]]
    if (~_remote.aliases.indexOf(host)) {
      remote = _remote
      host = registry.registry
      path = '/' + remote.name + path
      break
    }
  }

  // invalid hostname
  if (!~registry.aliases.indexOf(host)) return false

  // make sure the host is supported
  var frags = path.split('/')
  if (!remote) {
    var name = frags[1]
    for (var i = 0; i < names.length; i++) {
      _remote = remotes[names[i]]
      if (_remote.name === name || ~_remote.aliases.indexOf(name)) {
        remote = _remote
        break
      }
    }
  }

  if (!remote) return false

  // check user
  assert(!(!remote.namespace && frags[2] !== '-'), 'remotes without namespaces should have `-` as a user: ' + uri)
  // check project
  if (!frags[3]) return false

  // always lowercase user
  frags[2] = frags[2].toLowerCase()
  // lowercase project of the remote is case insensitive
  if (!remote.caseSensitive) frags[3] = frags[3].toLowerCase()

  frags[1] = remote.name
  frags.remote = remote
  frags.user = frags[2]
  frags.project = frags[3]
  frags.version = frags[4]
  // not even sure why i use these two anymore
  frags.filename = frags.slice(5).join('/')
  frags.pathname = frags.join('/')

  return frags
}
