
var assert = require('assert')

var Client = require('./client')

// get the versions of a `path`
Client.prototype.versions = function* (path) {
  if (path.slice(-1) !== '/') path += '/'
  var uri = path + 'versions.json'
  var res = yield* this.request(uri, 'json')
  var versions = res.body
  if (versions.length) return versions

  // if there are no versions,
  // let's assume no versions are installed on the proxy.
  // so let's install the latest one.
  var entrypoint = path + 'pull'
  var res = yield* this.request(entrypoint)
  res.destroy() // don't care about the response
  assert.equal(204, res.statusCode, '/pull got status code: ' + res.statusCode)
  res = yield* this.request(uri, 'json')
  return res.body
}
