
# Normalize Client

[![NPM version][npm-image]][npm-url]
[![Build status][travis-image]][travis-url]
[![Test coverage][coveralls-image]][coveralls-url]
[![Dependency Status][david-image]][david-url]
[![License][license-image]][license-url]
[![Downloads][downloads-image]][downloads-url]
[![Gittip][gittip-image]][gittip-url]

[npm-image]: https://img.shields.io/npm/v/normalize/client.js.svg?style=flat-square
[npm-url]: https://npmjs.org/package/normalize/client.js
[github-tag]: http://img.shields.io/github/tag/normalize/client.js.svg?style=flat-square
[github-url]: https://github.com/normalize/client.js/tags
[travis-image]: https://img.shields.io/travis/normalize/client.js.svg?style=flat-square
[travis-url]: https://travis-ci.org/normalize/client.js
[coveralls-image]: https://img.shields.io/coveralls/normalize/client.js.svg?style=flat-square
[coveralls-url]: https://coveralls.io/r/normalize/client.js?branch=master
[david-image]: http://img.shields.io/david/normalize/client.js.svg?style=flat-square
[david-url]: https://david-dm.org/normalize/client.js
[license-image]: http://img.shields.io/npm/l/normalize/client.js.svg?style=flat-square
[license-url]: LICENSE
[downloads-image]: http://img.shields.io/npm/dm/normalize/client.js.svg?style=flat-square
[downloads-url]: https://npmjs.org/package/normalize/client.js
[gittip-image]: https://img.shields.io/gittip/jonathanong.svg?style=flat-square
[gittip-url]: https://www.gittip.com/jonathanong/
